package DeretAngka;

import java.util.Scanner;

public class Soal06 {
    public static void Resolve(int n){
        Scanner input = new Scanner(System.in);

        int[] results = new int[n];
        int helper = 1;

        for (int i = 0; i < n; i++) {
            if(i % 3 == 2){
                System.out.print("* ");
                helper +=4;
            }else {
                results[i] = helper;
                helper +=4;
                System.out.print(results[i] + " ");
            }

        }
    }
}
