package ProblemSolving14;

import java.util.Scanner;

public class PenjumlahanAntarBaris01 {
    public static void nomor01(){

        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan panjang baris = ");
        int baris = input.nextInt();

        int[] baris1 = new int[baris];
        int[] baris2 = new int[baris];
        int[] baris3 = new int[baris];

        System.out.print("Baris pertama = ");
        for (int i = 0; i < baris; i++) {
            baris1[i] = ((i * 3) -1);
            System.out.print(baris1[i] + " ");
        }
        System.out.println();

        System.out.print("Baris kedua = ");
        for (int i = 0; i < baris; i++) {
            baris2[i] = (i * - 2);
            System.out.print(baris2[i] + " ");
        }
        System.out.println();

        System.out.print("Output = ");
        for (int i = 0; i < baris; i++) {
            baris3[i] = baris1[i] + baris2[i];
            System.out.print(baris3[i] + " ");
        }
    }
}
