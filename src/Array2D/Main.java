package Array2D;

import java.util.Scanner;
public class Main {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        int pilihan = 0;
        int n = 0;
        boolean flag = true;
//        String answer = "y";

        System.out.println("Pilih soal (1-13)");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 13){
            System.out.println("Pilihan Tidak Tersedia");
            pilihan = input.nextInt();
        }
        while (flag){
            switch (pilihan){
                case 1:
                    kali3.Resolve();
                    break;
                case 2:
                    kali3barismin.Resolve();
                    break;
                case 3:
                    soal03.Resolve();
                    break;
                case 4:
                    soal04.Resolve();
                    break;
                case 5:
                    urutangka.Resolve();
                    break;
                case 6:
                    penjumlahanbaris1dan2.Resolve();
                    break;
                case 7:
                    soal07.Resolve();
                    break;
                case 8:
                    soal08.Resolve();
                    break;
                case 9:
                    soal09.Resolve();
                    break;
                case 10:
                    soal10.Resolve();
                    break;
                case 11:
                    soal11.Resolve();
                    break;
                case 12:
                    soal12.Resolve();
                    break;
                case 13:
                    Example.Resolve();
                    break;

            }
            flag = false;
        }

    }

}
