package ProblemSolving14;

import java.util.Arrays;
import java.util.Scanner;

public class belanjadenganhargaterbesar08 {
    public static void nomor08(){
        Scanner input = new Scanner(System.in);

        System.out.println("Uang Andi: 70");
        System.out.println("Harga kacamata: 34, 26, 44");
        System.out.println("Harga baju: 21, 39, 33");
        System.out.println("Output = 67");

        System.out.println("masukkan uang andi yang ingin di belanjakan : ");
        int uangAndi = input.nextInt();
        input.nextLine();
        System.out.println("masukkan daftar harga kacamata : ");
        String harga1 = input.nextLine();
        System.out.println("masukkan daftar harga baju : ");
        String harga2 = input.nextLine();

        String[] jumlahHarga1 = harga1.split(", ");
        String[] jumlahHarga2 = harga2.split(", ");

        int[] kacamataArray1 = new int[jumlahHarga1.length];
        int[] bajuArray2 = new int[jumlahHarga2.length];

        for (int i = 0; i < kacamataArray1.length; i++) {
            kacamataArray1[i] = Integer.parseInt(jumlahHarga1[i]);
        }
        for (int i = 0; i < bajuArray2.length; i++) {
            bajuArray2[i] = Integer.parseInt(jumlahHarga2[i]);
        }

        int helper = 0;
        int helpertotalpembelian = 0;
        // deklarasi berapa jumlah harga yang bisa dimunculkan oleh barang
        int[] totalHarga = new int[jumlahHarga1.length * jumlahHarga2.length];

        for (int i = 0; i < jumlahHarga1.length; i++) {
            for (int j = 0; j < jumlahHarga2.length; j++) {
                totalHarga[helper] = kacamataArray1[i] + bajuArray2[j];
                helper++;
            }
        }
        Arrays.sort(totalHarga);

        for (int i = 0; i < totalHarga.length; i++) {
            if(totalHarga[i] <= uangAndi){
                helpertotalpembelian = totalHarga[i];
            }
        }
        System.out.println("Total barang termahal dari kacamata dan baju adalah = " + helpertotalpembelian);
    }
}


