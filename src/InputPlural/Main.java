package InputPlural;

import DeretAngka.*;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (1-13)");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 13){
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 1:
                Latihan01.Resolve();
                break;
            case 2:
                Latihan02.Resolve();
                break;
            case 3:
                Latihan03.Resolve();
                break;
//            case 4:
//                Soal04.Resolve(n);
//                break;
            case 5:
                Latihan05.Resolve();
                break;
            case 6:
                Latihan06.Resolve();
                break;
            case 7:
                Latihan07.Resolve();
                break;
            case 8:
                Latihan08.Resolve();
                break;
            case 9:
                Latihan09.Resolve();
                break;
            case 10:
                Latihan10.Resolve();
                break;
            case 11:
                Latihan11.Resolve();
                break;
//            case 12:
//                Latihan12.Resolve();
//                break;
            case 13:
                Example.Resolve();
                break;

        }

    }
}