package ProblemSolving14;

import java.util.Scanner;

public class Nomor07 {
    public static void nomor07(){
        Scanner input = new Scanner(System.in);

        System.out.println("contoh pola = ---o-o-");
        System.out.println("Masukkan lintasan = ");
        String lintasan = input.nextLine();

        System.out.println("contoh langkah = wwwjj ");
        System.out.println("W = jalan ");
        System.out.println("J = loncat");
        System.out.println("Masukkan langkah = ");
        String langkah = input.nextLine().toLowerCase();

        boolean jatuh = false;
        int energi = 0;
        int helper = 0;

        for (int i = 0; i < langkah.length(); i++) {
            if(langkah.charAt(i) == 'w' && lintasan.charAt(helper) == '-'){
                energi++;
                helper++;
            }else if (langkah.charAt(i) == 'w' && lintasan.charAt(helper) == 'o'){
                jatuh = true;
                break;
            } else if (langkah.charAt(i) == 'j' && energi >= 2) {
                helper +=2;
                if(langkah.charAt(helper - i) == 'o' ){
                    jatuh = true;
                    break;
                }else {
                    energi -=2;
                }
            } else if (langkah.charAt(i) == 'j' && lintasan.charAt(helper) == 'o' && energi < 2) {
                jatuh = true;
                break;
            }
        }
        if(jatuh){
            System.out.println("jim died");
        }
        if(!jatuh){
            System.out.println(energi);
        }

    }
}
