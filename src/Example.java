import java.util.Scanner;

public class Example {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.println("masukkan nilai n =");
        int n = input.nextInt();
        int[] results = new int[n];
        int tambah = 0;

        for (int i = 0; i < n; i++) {
            results[i] = tambah;
            tambah++;
            System.out.print(tambah + " ");
        }
    }
}
