package Basic;

import java.util.Scanner;

public class Persegi {
    private static Scanner input = new Scanner(System.in);
    private static int sisi;

    public static void Luas(){
        System.out.println("Input sisi (cm)");
        sisi = input.nextInt();

        int luas = sisi * sisi;

        System.out.println("luas persegi adalah = " + luas);
    }
    public static void Keliling(){
        System.out.println("Input sisi (cm)");
        sisi = input.nextInt();

        int keliling = 4 * sisi;

        System.out.println("Keliling persegi adalah = " + keliling);
    }
}
