package Array2D;

import DeretAngka.Unility;

import java.util.Scanner;
public class Example {

    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input n");
        int n = input.nextInt();

        int[][] results = new int[2][n];
        int ganjil = 1;
        int genap = 2;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if(i == 0){
                    results[i][j] = ganjil;
                    ganjil += 2;
                } else if (i == 1) {
                    results[i][j] = genap;
                    genap += 2;
                }

            }

        }
        Utility.PrintArray2D(results);

    }

}
