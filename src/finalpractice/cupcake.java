package finalpractice;

import java.util.Scanner;

public class cupcake {
    public static void nomor10(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan cupcake yang akan dibuat = ");
        int cupcake = input.nextInt();

        double terigu = (double) 125/15;
        double gula = (double) 100/15;
        double susu = (double) 100/15;

        int cupcakeTerigu = (int) (terigu * cupcake);
        int cupcakeGula = (int) (gula * cupcake);
        int cupcakeSusu = (int) (susu*cupcake);

        System.out.println("Bahan yang dibutuhkan untuk membuat" + cupcake + "cupcake adalah ");
        System.out.println("Terigu" + cupcakeTerigu + "gr");
        System.out.println("Gula" + cupcakeGula + "gr");
        System.out.println("susu" + cupcakeSusu + "gr");
    }
}
