package InputPlural;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Latihan09 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("-4 2 -5 0 1 10");
        System.out.println("output = Positif = 50%, negatif = 33.3%, zero = 16.6%");
        System.out.println("Masukkan bilangan yang akan di cari yang postif, negatif, dan 0 :");
        String text = input.nextLine();


        int[] intArray= Utility.ConvertStringToArrayInt(text);
        double lenght = intArray.length;
        double positif = 0;
        double negatif = 0;
        double zero = 0;

        for (int i = 0; i < lenght; i++) {
            if(intArray[i] > 0){
                positif++;
            } else if (intArray[i] < 0) {
                negatif++;
            } else if (intArray[i] == 0) {
                zero++;
            }

        }
        // memberi format pada nilai
        DecimalFormat desimal = new DecimalFormat("0.0");
        positif = ((positif/lenght) * 100);
        negatif = ((negatif/lenght) * 100);
        zero = ((zero/lenght) * 100);

        System.out.println(desimal.format(positif) + " %");
        System.out.println(desimal.format(negatif) + " %");
        System.out.println(desimal.format(zero) + " %");
    }
}
