package ProblemSolving14;

import java.util.Scanner;

public class JarakToko02 {
    public static void nomor02(){
        Scanner input = new Scanner(System.in);
        String jaraktoko = "0 0.5 2 3.5 5";

        int waktu = 0;
        double selisihjarak = 0;
        double jaraktempuh = 0 ;

        System.out.println("Jarak Toko = ");
        System.out.println("Toko 1 = 0.5 km");
        System.out.println("Toko 2 = 2 Km");
        System.out.println("Toko 3 = 3.5 Km");
        System.out.println("Toko 4 = 5 Km");
        System.out.println("Masukkan toko yang akan dikunjungi = ");
        String jumlah = input.nextLine();

        double[] jarakketoko = Utility.ConvertStringToArrayDouble(jaraktoko);
        int[] intArray = Utility.ConvertStringToArrayInt(jumlah);
        int lenght = intArray.length;

        for (int i = 0; i < lenght; i++) {
            if(i == 0){
                jaraktempuh = jarakketoko[intArray[i]];
            } else if (i == intArray.length -1) {
                selisihjarak = jarakketoko[intArray[i]] - jarakketoko[intArray[i - 1]];
                if(selisihjarak < 0 ){
                    selisihjarak *= -1;
                }
                jaraktempuh = jaraktempuh + selisihjarak + jarakketoko[intArray[i]];

            }else {
                selisihjarak = jarakketoko[intArray[i]]-jarakketoko[intArray[i - 1]];
                if(selisihjarak < 0 ){
                    selisihjarak *= -1;
                }
                jaraktempuh = jaraktempuh + selisihjarak;
            }

        }
        waktu = (int) ((jaraktempuh * 2) + (intArray.length *10));
        System.out.println("Waktu Tempuh = " + waktu + " Menit");
        System.out.println("Jarak Tempuh = " + jaraktempuh + " Km");

    }
}
