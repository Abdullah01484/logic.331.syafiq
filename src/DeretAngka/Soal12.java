package DeretAngka;

public class Soal12 {
    public static void Resolve(int n)
    {
        int prima = 0;
        int counter = 0;
        int [] results = new int[n];

        for (int i = 0; i < n * 10; i++) {
            for (int j = 1; j <= i; j++) {
                if (i % j == 0){
                    prima++;
                }
            }
            if (prima == 2){
                results[counter] = i;
                // menandai data output di isi berapa kali dan harus berhenti
                // ketika panjang data telah mncapai panjang yang di inputkan (n)
                counter++;
            }
            if (counter == n){
                break;
            }
            prima = 0;

        }

        Unility.PrintArray1D(results);
    }
}
