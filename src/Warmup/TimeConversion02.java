package Warmup;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class TimeConversion02 {
    public static void timeconversion(){
        Scanner input = new Scanner(System.in);

        System.out.println("contoh = 07:05:45 PM");
        System.out.println("Hasil : 19:05:45");
        System.out.println("Masukkan jam : ");
        String jam12 = input.nextLine();


        SimpleDateFormat waktu = new SimpleDateFormat("hh:mm:ss a");

        // menangani pengecualian
        try {
            // Menguraikan jam dalam format 12 jam menjadi objek Date.
            Date date = waktu.parse(jam12);
            //Membuat objek SimpleDateFormat untuk memformat waktu dalam format 24 jam.
            SimpleDateFormat waktu1 = new SimpleDateFormat("HH:mm:ss");

            // Mengonversi waktu ke format 24 jam.
            String jam24 = waktu1.format(date);

            // Menampilkan waktu dalam format 24 jam.
            System.out.println("Jam dalam format 24 jam: " + jam24);

        } catch (ParseException e) {
            System.out.println("format jam tidak valid");
        }


    }
}
