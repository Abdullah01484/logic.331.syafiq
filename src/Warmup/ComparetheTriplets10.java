package Warmup;

import java.util.Scanner;

public class ComparetheTriplets10 {
    public static void comparthetriplets(){
        Scanner input = new Scanner(System.in);
        System.out.println("baris pertama : 5 6 7");
        System.out.println("baris kedua : 3 6 10");
        System.out.println("output = 1 1");
        System.out.println("Masukkan bilangan yang akan di bandingkan");
        System.out.println("Baris pertama : ");
        String baris1 = input.nextLine();
        System.out.println("Baris kedua : ");
        String baris2 = input.nextLine();

        int barispertama = 0;
        int bariskedua = 0;

        int[] hasil1 = Utility.ConvertStringToArrayInt(baris1);
        int[] hasil2 = Utility.ConvertStringToArrayInt(baris2);

        for (int i = 0; i < hasil1.length; i++) {
            if (hasil1[i] > hasil2[i]){
                barispertama++;
            } else if (hasil1[i] < hasil2[i]){
                bariskedua++;
            }

        }
        System.out.println("Hasil = " + barispertama + " " + bariskedua);
    }
}
