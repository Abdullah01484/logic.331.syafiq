package ProblemSolving14;

import java.util.Scanner;

public class Nomor11 {
    public static void nomor11(){
        Scanner input = new Scanner(System.in);

        System.out.println("Beli pulsa = ");
        int pulsa = input.nextInt();
        int point = 0;

        if(pulsa <= 10000){
            point = 0;
        }
        else if (pulsa > 10000 && pulsa < 30000){
            point = (pulsa - 10000) / 1000;
        }
        else{
            point = ((pulsa - 30000) /1000) *2;
            point += 20;
        }
        System.out.println("Point yang didapatkan = " + point);
    }
}
