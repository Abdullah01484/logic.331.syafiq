package Array2D;

import java.util.Scanner;

public class kali3barismin {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("input n");
        int n = input.nextInt();

        int[][] results = new int[2][n];
        int tambah = 0;
        int kali = 1;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if(i == 0){
                    results[i][j] = tambah;
                    tambah++;
                } else if (i == 1) {
                    if(j % 3 == 2){
                        kali *= -1;
                        results[i][j] = kali;
                        kali *= -3;
                    }else{
                        results[i][j] = kali;
                        kali *= 3;
                    }
                }
            }
            
        }
        Utility.PrintArray2D(results);
    }
}
