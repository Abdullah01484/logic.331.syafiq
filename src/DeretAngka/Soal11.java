package DeretAngka;

public class Soal11 {
    public static void Resolve(int n){
        int[] array1d = new int[n];

        array1d[0] = 1;
        array1d[1] = 1;

        for (int i = 2; i < n; i++) {
            array1d[i] = array1d[i - 1] + array1d[i - 2];
        }

        for (int i = 0; i < n; i++) {
            System.out.print(array1d[i] + " ");
        }
    }

}
