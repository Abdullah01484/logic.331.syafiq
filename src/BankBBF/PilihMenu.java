package BankBBF;

import java.util.Scanner;

public class PilihMenu {
    private static Scanner input = new Scanner(System.in);
    private static int pilihmenu;

    public static void menu() {
        boolean flag = true;
        while (flag){
            System.out.println("=== Pilih Menu ===");
            System.out.println("1. Setor Tunai");
            System.out.println("2. Transfer");
            System.out.println("3. Keluar");
            System.out.println("Pilihan = ");
            pilihmenu = input.nextInt();

            if (pilihmenu == 1){
                SetorTunai.EmpatSatu();
                flag = false;
            } else if (pilihmenu == 2) {
                Transfer.EmpatDua();
                flag = false;
            } else {
                flag = false;
                break;
            }

        }


    }

}
