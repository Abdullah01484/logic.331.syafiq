package DeretAngka;

import InputPlural.Example;

import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (1-12)");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 12){
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }
        System.out.println("Masukkan nilai n: ");
        n = input.nextInt();

        switch (pilihan)
        {
            case 1:
                Soal01.Resolve(n);
                break;
            case 2:
                Soal02.Resolve(n);
                break;
            case 3:
                Soal03.Resolve(n);
                break;
//            case 4:
//                Soal04.Resolve(n);
//                break;
            case 5:
                Soal05.Resolve(n);
                break;
            case 6:
                Soal06.Resolve(n);
                break;
            case 7:
                Soal07.Resolve(n);
                break;
            case 8:
                Soal08.Resolve(n);
                break;
            case 9:
                Soal09.Resolve(n);
                break;
            case 10:
                Soal10.Resolve(n);
                break;
            case 11:
                Soal11.Resolve(n);
                break;
            case 12:
                Soal12.Resolve(n);
                break;
        }

    }


}
