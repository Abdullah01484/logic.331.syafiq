package String;

import java.util.Scanner;

public class CaesarCipher {
    public static void caesarcipher(){
        Scanner input =new Scanner(System.in);

        System.out.println("Masukkan kalimat = ");
        String kalimat = input.nextLine();
        System.out.println("masukan jumlah loncat kalimat = ");
        int jumlah =  input.nextInt();
        // digunakan untuk membalikkan angka lebih dari 26
        if (jumlah > 26){
            jumlah = jumlah % 26;
        }
        // tolowercase merubah menjadi huruf kecil
        char[] huruf = kalimat.toLowerCase().toCharArray();

        for (int i = 0; i < huruf.length; i++) {
            if (huruf[i] >= 'a' && huruf[i] <= 'z'){
                huruf[i] += jumlah;
                if (huruf[i] > 'z'){
                    huruf[i] -= 26;
                }
            }
        }
        System.out.println(huruf);

    }
}
