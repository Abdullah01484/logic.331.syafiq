package PreTest;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int pilihan = 0;
        int n = 0;

        System.out.println("Masukkan nomor soal = ");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 12){
            System.out.println("Silahkan masukkan nomor soal lagi");
            pilihan = input.nextInt();
        }
        switch (pilihan){
            case 1:
                GanjilGenap.soal01();
                break;
            case 4:
                AntarToko.soal04();
                break;
            case 5:
                PorsiMakan.soal05();
                break;
            case 6:
                TransferDanSetorTunai.soal06();
                break;
            case 8:
                penjumlahanbaris1dan2.nomor08();
                break;
            case 9:
                naikturungunung.nomor09();
                break;
        }

    }
}