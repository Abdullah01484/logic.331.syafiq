package ProblemSolving14;

import java.util.Scanner;

public class Nomor10 {
    public static void nomor10(){
        Scanner input = new Scanner(System.in);
        String wadah = "botol, teko, gelas, cangkir";

        System.out.println("Mengkonversi botol atau teko atau gelas menjadi cangkir");
        System.out.println("1 botol = 2 gelas, 1 teko = 25 cangkir, 1 gelas = 2.5 cangkir");

        System.out.println("Masukkan botol atau teko atau gelas atau cangkir = ");
        String jenisinput = input.nextLine();
        System.out.println("Masukkan Jumlahnya");
        int jumlahwadah = input.nextInt();
        input.nextLine();
        System.out.println("konversi wadah menjadi : ");
        String wadah1 = input.nextLine();
        double hasilKonversi = 0;

        String[] wadahArray = wadah.split(", ");


        for (int i = 0; i < wadahArray.length; i++) {
            if(jenisinput.equals(wadahArray[i])){
                if (i == 0 && wadah1.equals(wadahArray[0])){
                    hasilKonversi = jumlahwadah;
                } else if (i == 0 && wadah1.equals(wadahArray[1])) {
                    hasilKonversi = (double) jumlahwadah/5;
                } else if (i == 0 && wadah1.equals(wadahArray[2])) {
                    hasilKonversi = (double) jumlahwadah*2;
                } else if (i == 0 && wadah1.equals(wadahArray[3])) {
                    hasilKonversi = (double) jumlahwadah*5;
                }
                if (i == 1 && wadah1.equals(wadahArray[1])){
                    hasilKonversi = jumlahwadah;
                } else if (i == 1 && wadah1.equals(wadahArray[0])) {
                    hasilKonversi = (double) jumlahwadah/5;
                } else if (i == 1 && wadah1.equals(wadahArray[2])) {
                    hasilKonversi = (double) jumlahwadah/10;
                } else if (i == 1 && wadah1.equals(wadahArray[3])) {
                    hasilKonversi = (double) jumlahwadah*25;
                }
                if (i == 2 && wadah1.equals(wadahArray[2])){
                    hasilKonversi = jumlahwadah;
                } else if (i == 2 && wadah1.equals(wadahArray[0])) {
                    hasilKonversi = (double) jumlahwadah/2;
                } else if (i == 2 && wadah1.equals(wadahArray[1])) {
                    hasilKonversi = (double) jumlahwadah*10;
                } else if (i == 2 && wadah1.equals(wadahArray[3])) {
                    hasilKonversi = (double) jumlahwadah*2.5;
                }
                if (i == 3 && wadah1.equals(wadahArray[3])){
                    hasilKonversi = jumlahwadah;
                } else if (i == 3 && wadah1.equals(wadahArray[0])) {
                    hasilKonversi = (double) jumlahwadah*0.2;
                } else if (i == 3 && wadah1.equals(wadahArray[1])) {
                    hasilKonversi = (double) jumlahwadah*0.04;
                } else if (i == 3 && wadah1.equals(wadahArray[2])) {
                    hasilKonversi = (double) jumlahwadah*0.4;
                }
            }
        }
        System.out.println("Hasil konversi : " + hasilKonversi);

    }
}
