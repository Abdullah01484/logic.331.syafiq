package Basic;

import java.util.Scanner;

public class Lingkaran {

    private static Scanner input = new Scanner(System.in);
    private static int JariJari;
    private static int phi = 22/7;

    public static void Luas(){
        System.out.println("Input jarijari (cm)");
        JariJari = input.nextInt();

        int luas = phi * JariJari * JariJari;

        System.out.println("luas lingkaran adalah = " + luas);
    }
    public static void Keliling(){
        System.out.println("Input Jarijari (cm)");
        JariJari = input.nextInt();

        int keliling = 2 * phi * JariJari;

        System.out.println("Keliling lingkaran adalah = " + keliling);
    }
}
