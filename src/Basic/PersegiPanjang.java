package Basic;

import java.util.Scanner;

public class PersegiPanjang {
    private static Scanner input = new Scanner(System.in);
    private static int panjang;
    private static int lebar;

    public static void Luas(){
        System.out.println("Input Panjang (cm)");
        panjang = input.nextInt();

        System.out.println("Input Lebar (cm)");
        lebar = input.nextInt();

        int luas = panjang * lebar;
        System.out.println("Luas persegi panjang adalah = " + luas);
    }
    public static void Keliling(){
        System.out.println("Input Panjang (cm)");
        panjang = input.nextInt();

        System.out.println("Input Lebar (cm)");
        lebar = input.nextInt();

        int keliling = (2 * panjang) + (2 * lebar);
        System.out.println("Keliling persegi panjang adalah = " + keliling);
    }
}
