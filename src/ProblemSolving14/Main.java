package ProblemSolving14;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (1-14)");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 14){
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 1:
                PenjumlahanAntarBaris01.nomor01();
                break;
            case 2:
                JarakToko02.nomor02();
                break;
            case 3:
                Nomer03.nomer03();
                break;
            case 4:
                Nomer04.nomer04();
                break;
            case 5:
                formatjam05.nomor05();
                break;
//            case 6:
//                Latihan06.Resolve();
//                break;
            case 7:
                Nomor07.nomor07();
                break;
            case 8:
                belanjadenganhargaterbesar08.nomor08();
                break;
            case 9:
                Tarifparkir09.nomor09();
                break;
            case 10:
                Nomor10.nomor10();
                break;
            case 11:
                Nomor11.nomor11();
                break;
            case 12:
                Nomor12.nomer12();
                break;
            case 13:
                Nomor13.nomor13();
                break;

        }

    }
}