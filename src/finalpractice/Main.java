package finalpractice;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;

        System.out.println("Masukkan nomor soal 1-10 : ");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 10){
            System.out.println("nomor tidak tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan){
            case 1:
                EsLoliBambang.nomor01();
                break;
            case 3:
                rotasi.nomor03();
                break;
            case 5:
                merubahwaktu.nomor05();
                break;
            case 7:
                MeanModusMedian.nomor07();
                break;
            case 10:
                cupcake.nomor10();
                break;
        }
    }
}