package ProblemSolving14;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Nomor12 {
    public static void nomer12(){
        Scanner input = new Scanner(System.in);

        System.out.println("Penjumlahan Buah = ");
        System.out.println("Apel:1, Pisang:3, Jeruk:1, Apel:3, Apel:5, Jeruk:8, Mangga:1");
        System.out.println("Masukkan buah dan jumlahnya = ");
        String buah = input.nextLine();
        String [] jumlahbuah = buah.split(", ");
        String[] items = new String[2];
        int helper = 0;

        HashMap<String,Integer> banyakbuah = new HashMap<String,Integer>();

        for (int i = 0; i < jumlahbuah.length; i++) {
            items = jumlahbuah[i].split(":");
//            banyakbuah.put(items[0],Integer.parseInt(items[1]));
            if(banyakbuah.get(items[0]) == null){
                banyakbuah.put(items[0], Integer.parseInt(items[1]));
            } else {
                helper= banyakbuah.get(items[0]);
                banyakbuah.put(items[0], Integer.parseInt(items[1])+ helper);
            }
        }

        String[] results = new String[banyakbuah.size()];

        int index = 0;

        for (String key : banyakbuah.keySet()){
            results[index] = key + ": " + banyakbuah.get(key);
            index++;
        }
        Arrays.sort(results);

        for (int i = 0; i < banyakbuah.size(); i++) {
            System.out.println(results[i]);
        }

    }
}
