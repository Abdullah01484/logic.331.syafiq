package Basic;

import java.util.Scanner;

public class Trapesium {

    private static Scanner input = new Scanner(System.in);
    private static int alasA;
    private static int alasB;
    private static int sisiA;
    private static int sisiB;
    private static int tinggiA;

    public static void Luas(){
        System.out.println("Input AlasA (cm)");
        alasA = input.nextInt();

        System.out.println("Input AlasB (cm)");
        alasB = input.nextInt();

        System.out.println("Input TinggiA (cm)");
        tinggiA = input.nextInt();

        int luas = tinggiA * (alasA * alasB) / 2;

        System.out.println("luas trapesium adalah = " + luas);
    }
    public static void Keliling(){
        System.out.println("Input AlasA (cm)");
        alasA = input.nextInt();

        System.out.println("Input AlasB (cm)");
        alasB = input.nextInt();

        System.out.println("Input SisiA (cm)");
        sisiA = input.nextInt();

        System.out.println("Input SisiB (cm)");
        sisiB = input.nextInt();

        int keliling = alasA + alasB + sisiA + sisiB;

        System.out.println("Keliling trapesium adalah = " + keliling);
    }
}
