package DeretAngka;

public class Soal07 {
    public static void Resolve(int n)
    {
        int kali = 2;
        int [] results = new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = kali;
            kali *= 2;
        }

        Unility.PrintArray1D(results);
    }
}
