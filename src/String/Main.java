package String;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (1-10)");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 10){
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 1:
                CamelCase.CamalCase();
                break;
//            case 2:
//                soal02.Resolve();
//                break;
            case 3:
                CaesarCipher.caesarcipher();
                break;
            case 4:
                MarsExploration.marsexploration();
                break;
//            case 5:
//                soal05.Resolve();
//                break;
//            case 6:
//                soal06.Resolve();
//                break;
//            case 7:
//                soal07.Resolve();
//                break;
//            case 8:
//                soal08.Resolve();
//                break;
//            case 9:
//                soal09.Resolve();
//                break;
//            case 10:
//                soal10.Resolve();
//                break;
        }
    }

}
