package DeretAngka;

import java.util.Scanner;

public class Soal09 {
    public static void Resolve(int n){
        Scanner input = new Scanner(System.in);

        int helper = 4;
        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            if(i % 3 == 2){
                System.out.print("* ");
            }else {
                results[i] = helper;
                helper *= 4;
                System.out.print(results[i] + " ");
            }
        }
    }

}
