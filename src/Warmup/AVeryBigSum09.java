package Warmup;

import java.util.Scanner;

public class AVeryBigSum09 {
    public static void averybigsum(){
        Scanner input = new Scanner(System.in);
        System.out.println("1000000001 1000000002 1000000003 1000000004 1000000005");
        System.out.println("output = 5000000015");
        System.out.println("Masukkan bilangan yang akan di tambahkan :");
        String text = input.nextLine();
        long hasil = 0;

        String[] nilai = text.split(" ");
        long[] intBigNum = new long[nilai.length];
        for (int i = 0; i < nilai.length; i++) {
            intBigNum[i] = Long.parseLong(nilai[i]);
            hasil += Long.parseLong(nilai[i]);
        }

        System.out.print("Output = ");
        System.out.println(hasil);

    }
}
