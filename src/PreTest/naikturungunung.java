package PreTest;

import java.util.Scanner;

public class naikturungunung {
    public static void nomor09(){
        Scanner input = new Scanner(System.in);

        System.out.println("contoh = N N T T");
        System.out.println("Masukkan N & T = ");
        String rute = input.nextLine();

        //menghilangkan spasi antar text
        String rutePenghilang = rute.trim().toLowerCase();
        char[] ruteArray = rutePenghilang.toCharArray();

        int naik = 0;
        int turun = 0;
        int ketinggian = 0;

        int countgunung = 0;
        int countlembah = 0;

        for (int i = 0; i < ruteArray.length; i++) {
            if(ruteArray[i] == 'n'){
                naik++;
            } else if (ruteArray[i] == 't') {
                turun++;
            }
            ketinggian = naik - turun;

            if(ketinggian == 0 && ruteArray[i] == 't'){
                countgunung++;
            } else if (ketinggian== 0 && ruteArray[i] == 'n') {
                countlembah++;
            }
        }
        System.out.println("Jumlah Gunung = " + countgunung);
        System.out.println("Jumlah Lembah = " + countlembah);
    }
}
