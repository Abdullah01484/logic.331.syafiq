import java.util.Scanner;

public class Example1 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.println("Input n");
        int n = input.nextInt();

        int[] results = new int[n];
        int kelipatan = -2;

        for (int i = 0; i < n; i++) {
            results[i]= kelipatan;
            kelipatan = (i * 2) + 1;
            System.out.print(results[i] + " ");

        }


    }
}