package BankBBF;

import java.util.Scanner;

public class SetorTunai {
    private static Scanner input = new Scanner(System.in);
    private static boolean flag = true;
    private static int saldo;

    public static void EmpatSatu(){
        flag = true;

        while (flag){
            System.out.println("Silahkan masukkan nominal setor tunai :");
            String ceksetortunai = input.nextLine();
            Utility.IsNumeric(ceksetortunai);
            int setortunai = Integer.parseInt(ceksetortunai);

            if (setortunai >= 25000000){
                System.out.println("Maaf setor tunai anda melebihi batas yaitu 25.000.000");
            }
            else {
                System.out.println("Apakah anda yakin dengan uang setor tunai anda ? ");
                String jawaban = input.nextLine();
                if(!jawaban.toLowerCase().equals("y")){
                    System.out.println("Silakan setor tunai kembali");
                }
                else {
                    System.out.println("Uang setor tunai anda sudah kami terima");
                    saldo = setortunai;
                    System.out.println("Apakah anda ingin lanjut transaksi ?");
                    String jawab = input.nextLine();
                    if(!jawab.toLowerCase().equals("y")){
                        System.out.println("Terima kasih");
                        flag = false;
                    }
                    else{
                        flag = false;
                        Login.Masuk();
                    }
                }
            }

        }

    }

    public static int getSaldo(){
        return saldo;
    }

}
