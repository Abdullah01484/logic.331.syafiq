package ProblemSolving14;

import java.util.Scanner;

public class Nomor13 {
    public static void nomor13(){
        Scanner input = new Scanner(System.in);

        System.out.println("masukkan huruf dalam huruf kecil : ");
        String hurufkecil = input.nextLine();
        char[] huruf = hurufkecil.toCharArray();

        System.out.println("masukkan angka : ");
        String angka = input.nextLine();
        String [] angkapembatas = angka.split(", ");
        int[] angkaArray = new int[angkapembatas.length];

        for (int i = 0; i < angkapembatas.length; i++) {
            angkaArray[i] = Integer.parseInt(angkapembatas[i]);
        }
        // untuk menentukan true dan false
        String[] output = new String[huruf.length];

        for (int i = 0; i < angkaArray.length; i++) {
            if(huruf[i] - 96 == angkaArray[i]){
                output[i] = "true";
            } else{
                output[i] = "false";
            }
        }
        for (int i = 0; i < angkaArray.length; i++) {
            System.out.println("Output = " + output[i]);
        }
        System.out.println();
    }
}
