package Array2D;

import java.util.Scanner;

public class soal12 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Input n");
        int n = input.nextInt();


        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - i; j++) {
                System.out.print("  ");

            }
            for (int k = 0; k <= i*2; k++) {
                if(k % 2 == 0){
                    System.out.print("0 ");
                }
                else {
                    System.out.print("1 ");
                }

            }
            System.out.println();

        }

    }
}
