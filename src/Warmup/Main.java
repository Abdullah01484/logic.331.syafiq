package Warmup;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (1-10)");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 10){
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 1:
                SolveMeFirst01.solvemefirst();
                break;
            case 2:
                TimeConversion02.timeconversion();
                break;
            case 3:
                SimpleArraySum03.simplearraysum();
                break;
            case 4:
                DimensionCaseStudy04.dimensioncasestudy();
                break;
            case 5:
                PlusMinus05.Resolve();
                break;
            case 6:
                Staircase06.staircase();
                break;
            case 7:
                MinMaxSum07.minmaxsum();
                break;
            case 8:
                BirthdayCakeCandles08.birthdaycakecandles();
                break;
            case 9:
                AVeryBigSum09.averybigsum();
                break;
            case 10:
                ComparetheTriplets10.comparthetriplets();
                break;
        }
    }
}