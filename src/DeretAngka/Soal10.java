package DeretAngka;

import java.util.Scanner;

public class Soal10 {
    public static void Resolve(int n){
        Scanner input = new Scanner(System.in);

        int helper = 3;
        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            if(i % 4 == 3){
                System.out.print("xxx ");
                helper *=3;
            }else {
                results[i] = helper;
                helper *= 3;
                System.out.print(results[i] + " ");
            }
        }
    }
}
