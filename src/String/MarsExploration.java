package String;

import java.util.Scanner;

public class MarsExploration {
    public static void marsexploration(){
        Scanner input =new Scanner(System.in);

        System.out.println("Masukkan kalimat sos= ");
        String kalimat = input.nextLine();
        int count = 0;
        // tolowercase merubah menjadi huruf kecil
        char[] huruf = kalimat.toLowerCase().toCharArray();

        for (int i = 0; i < huruf.length; i+=3) {
            if (huruf[i] == 's' && huruf[i + 1] == 'o' && huruf [i + 2] == 's'){
                count++;
            }
        }
        System.out.println(count);

    }
}
