package PreTest;

import Array2D.Utility;

import java.util.Scanner;

public class GanjilGenap {
    public static void soal01(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan nilai n = ");
        int n = input.nextInt();

        int ganjil = -1;
        int genap = 0;
        int result[] = new int[n];

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n/2; j++) {
                if(i == 0){
                    result[i] = ganjil;
                    ganjil +=2;
                    System.out.print(ganjil + " ");
                }else if (i == 1){
                    result[j] = genap;
                    genap +=2;
                    System.out.print(genap + " ");
                }
            }
            System.out.println();
        }
    }
}
