package DeretAngka;

public class Soal08 {
    public static void Resolve(int n)
    {
        int kali = 3;
        int [] results = new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = kali;
            kali *= 3;
        }

        Unility.PrintArray1D(results);
    }
}
