package Basic;

import java.util.Scanner;

public class Segitiga {
    private static Scanner input = new Scanner(System.in);
    private static int alas;
    private static int tinggi;
    private static int sisimiring;

    public static void Luas(){
        System.out.println("Input alas (cm)");
        alas = input.nextInt();

        System.out.println("Input tinggi (cm)");
        tinggi = input.nextInt();

        int luas = (alas * tinggi) /2;
        System.out.println("Luas segitiga adalah = " + luas);
    }
    public static void Keliling(){
        System.out.println("Input alas (cm)");
        alas = input.nextInt();

        System.out.println("Input tinggi (cm)");
        tinggi = input.nextInt();

        System.out.println("Input sisi miring (cm)");
        sisimiring = input.nextInt();

        int Keliling = alas * tinggi * sisimiring;
        System.out.println("Keliling segitiga adalah = " + Keliling);
    }

}
