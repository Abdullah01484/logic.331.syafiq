package Warmup;

import InputPlural.Utility;

import java.text.DecimalFormat;
import java.util.Scanner;

public class PlusMinus05 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("-4 3 -9 0 4 1");
        System.out.println("output = Positif = 0.50000, negatif = 0.333333, zero = 0.166667%");
        System.out.println("Masukkan bilangan yang akan di cari yang postif, negatif, dan 0 :");
        String text = input.nextLine();


        int[] intArray= Utility.ConvertStringToArrayInt(text);
        double lenght = intArray.length;
        double positif = 0;
        double negatif = 0;
        double zero = 0;

        for (int i = 0; i < lenght; i++) {
            if(intArray[i] > 0){
                positif++;
            } else if (intArray[i] < 0) {
                negatif++;
            } else if (intArray[i] == 0) {
                zero++;
            }

        }
        // memberi format pada nilai
        DecimalFormat desimal = new DecimalFormat("0.000000");
        positif = (positif/lenght);
        negatif = (negatif/lenght);
        zero = (zero/lenght);

        System.out.println(desimal.format(positif));
        System.out.println(desimal.format(negatif));
        System.out.println(desimal.format(zero));
    }

}
