package Array2D;

import java.util.Scanner;

public class soal03 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input n");
        int n = input.nextInt();

        int[][] results = new int[2][n];
        int tambah = 0;
        int tambahkelipatan = 3;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if(i == 0){
                    results[i][j] = tambah;
                    tambah ++;
                } else if ( i == 1){
                    // input ganjil
                    if (n % 2 != 0 ) {
                        if(j < (n/2)){
                        results[i][j] = tambahkelipatan;
                        tambahkelipatan *= 2;
                        }
                        else if (j == (n/2)) {
                        results[i][j] = tambahkelipatan;
                        }
                        else if (j > (n/2)) {
                        tambahkelipatan /= 2;
                        results[i][j] = tambahkelipatan;
                        }
                    }
                    else{
                        // input genap
                        if(j < (n/2) -1){
                        results[i][j] = tambahkelipatan;
                        tambahkelipatan *= 2;
                        }
                        else if (j == n/2 || j == (n/2) -1) {
                        results[i][j] = tambahkelipatan;
                        }
                        else if (j > (n/2) -1) {
                        tambahkelipatan /= 2;
                        results[i][j] = tambahkelipatan;
                        }
                    }

                }

            }

        }
        Utility.PrintArray2D(results);

    }
}
