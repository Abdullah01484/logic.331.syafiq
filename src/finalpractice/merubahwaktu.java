package finalpractice;

import java.util.Scanner;

public class merubahwaktu {
    public static void nomor05(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan jam = ");
        String jam = input.nextLine().toUpperCase();

        String jamString = jam.substring(0,2);
        int jamInt = Integer.parseInt(jamString);

        //contains(mengandung nilai AM)
        if(jam.contains(("AM"))){
            if(jamInt == 12){
                jamInt = jamInt - 12;
                String convertIntToString = Integer.toString(jamInt);
                jam = jam.replace(jamString,convertIntToString);
                jam = jam.replace("AM","");
                System.out.println(jam);
            } else if (jamInt < 12) {
                jam = jam.replace("AM","");
                System.out.println(jam);
            }else {
                System.out.println("masukkan jam dengan benar");
            }
        } else if (jam.contains("PM")){
            jamInt = jamInt + 12;
            if (jamInt < 24) {
                String convertIntToString = Integer.toString(jamInt);
                jam = jam.replace(jamString,convertIntToString);
                jam = jam.replace("PM","");
                System.out.println(jam);
            } else if (jamInt == 24) {
                jam = jam.replace(jamString, "00");
                jam = jam.replace("PM","");
                System.out.println(jam);
            }else {
                System.out.println("masukkan jam dengan benar");
            }
        }


    }
}
