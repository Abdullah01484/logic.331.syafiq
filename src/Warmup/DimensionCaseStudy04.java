package Warmup;

import java.util.Scanner;

public class DimensionCaseStudy04 {
    public static void dimensioncasestudy() {

        Scanner input = new Scanner(System.in);

        System.out.println("contoh maskkan 3 baris");
        System.out.println("Baris 1 = 1 2 3");
        System.out.println("Baris 2 = 4 5 6");
        System.out.println("Baris 3 = 7 8 9");
        System.out.println("Masukkan Baris 1 : ");
        String baris1 = input.nextLine();
        System.out.println("Masukkan Baris 2 : ");
        String baris2 = input.nextLine();
        System.out.println("Masukkan Baris 3 : ");
        String baris3 = input.nextLine();

        int[] input1 = Utility.ConvertStringToArrayInt(baris1);
        int[] input2 = Utility.ConvertStringToArrayInt(baris2);
        int[] input3 = Utility.ConvertStringToArrayInt(baris3);

        int output = 0;

        int hasil1 = input1[0] + input2[1] + input3[2];
        int hasil2 = input1[2] + input2[1] + input3[0];

        if (hasil1 > hasil2) {
            output = hasil1 - hasil2;
        } else {
            output = hasil2 - hasil1;
        }
        System.out.println("hasil = " + output);
    }
}






