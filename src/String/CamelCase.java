package String;

import java.util.Scanner;

public class CamelCase {
    public static void CamalCase(){
        Scanner input = new Scanner(System.in);

        System.out.println("contoh : OneTwoThree");
        System.out.println("Output = 3");
        System.out.println("Masukkan kalimat yang akan dihitung katanya:");
        String kata = input.nextLine();
        int count = 1;

        // char berfunsi merubah string ke chart (huruf)
        char[] kalimat = kata.toCharArray();

        for (int i = 0; i < kalimat.length; i++) {
            // digunakan untuk memeriksa apakah suatu karakter (ch) adalah huruf besar (uppercase)
            if (Character.isUpperCase(kalimat[i])){
                count++;
            }
        }
        System.out.println("Jumlah kata: " +count);
    }

}
