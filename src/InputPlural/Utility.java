package InputPlural;

public class Utility {
    public static int[] ConvertStringToArrayInt(String text){
        String[] textArray = text.split( " ");
        int[] intArray = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            intArray[i] = Integer.parseInt(textArray[i]);

        }
        return intArray;
    }
    public static String[] ConvertStringToArrayString(String text){
        String[] textArray = text.split( " ");
        String[] StringArray = new String[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            StringArray[i] = (textArray[i]);

        }
        return StringArray;
    }

}
